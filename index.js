/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:


	let greetings = "Hello World";
	console.log("Hello World");


	let id1 = "Franz Jozef";
	let id2 = "Golingco";
	let age1 ="23";
	let hobbies1 = ["Chess", "Volleyball", "Cycling"];
	let address1 = ["Zone 5" , "Tabaco City", "Albay", "Philippines" ];

	console.log("Hi I'm" + ' ' + id1 + ' ' + id2);
	console.log(age1 + ' ' + "years of age.");
	console.log("Currently working at" + ' ' + address1);
	console.log("and I love to play / do");
	console.log(hobbies1);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is" + ' ' + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint" , "Nick"];
	console.log("My Friends are:")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}

	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
